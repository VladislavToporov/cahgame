package main.java.gameserverCAH.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class Repo implements RepoInterface {
    private Iterator<String> qIter;
    private Iterator<String> aIter;

    public Repo() {
        List<String> q = fillQuestions();
        List<String> a = fillAnswers();

        Collections.shuffle(q);
        qIter = q.iterator();

        Collections.shuffle(a);
        aIter = a.iterator();
    }

    public Iterator<String> getqIter() {
        return qIter;
    }

    public Iterator<String> getaIter() {
        return aIter;
    }

    @Override
    public String choiceQ() {
        String res = qIter.next();
        return res == null ? "blank" : res;
    }

    @Override
    public String choiceC() {
        String res = aIter.next();
        return res == null ? "blank" : res;
    }

    @Override
    public String choiceA() {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            try {
                String res = aIter.next();
                result.append(res == null ? "blank" : res);
                result.append("|");
            } catch (NoSuchElementException e) {
                System.exit(0);
            }

        }
        return result.toString();
    }

    @Override
    public List<String> fillQuestions() {
        List<String> q = new ArrayList<>();
        //File questions = new File("E:/IntellijIdea/cah/res/q.txt");
        //URL url = new URL("jar:file:/E:/IntellijIdea/cah/out/artifacts/cah_jar/cah.jar!/q.txt");
        //URL url = this.getClass().getResource("resources/q.txt");
        try {
            InputStream inputStream = ClassLoader.getSystemResourceAsStream("resourses/q.txt");
            InputStreamReader streamReader = new InputStreamReader(inputStream, "UTF-8");
            Scanner sc1 = new Scanner(streamReader);
            while (sc1.hasNext()) {
                q.add(sc1.nextLine());
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return q;
    }

    @Override
    public List<String> fillAnswers() {
        List<String> a = new ArrayList<>();
        //File answers = new File("E:/IntellijIdea/cah/res/a.txt");
        try {
            InputStream inputStream = ClassLoader.getSystemResourceAsStream("resourses/a.txt");
            InputStreamReader streamReader = new InputStreamReader(inputStream, "UTF-8");
            Scanner sc1 = new Scanner(streamReader);
            while (sc1.hasNext()) {
                a.add(sc1.nextLine());
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return a;
    }
}
