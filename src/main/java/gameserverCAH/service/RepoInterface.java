package main.java.gameserverCAH.service;

import java.util.List;

public interface RepoInterface {
    String choiceQ();

    String choiceC();

    String choiceA();

    List<String> fillQuestions();

    List<String> fillAnswers();


}
