package main.java.gameserverCAH.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;


public class Server {

    private final List<Connection> connections;
    private final int N;
    private Map<String, Integer> points;
    private ServerSocket server;
    private int token;
    private Map<String, String> answerPull;
    private volatile boolean flag = true;


    public Server(int n, int port) {
        if (n < 2 || n > 9)
            throw new IllegalArgumentException("Некорректное число игроков");
        if (port <= 0)
            throw new IllegalArgumentException("Некорректный порт");
        this.N = n;
        connections = Collections.synchronizedList(new ArrayList<>());
        answerPull = new HashMap<>();
        points = new HashMap<>();
        try {
            server = new ServerSocket(port);
            while (connections.size() < N) {
                Socket socket = server.accept();
                Connection connection = new Connection(socket, this);
                connections.add(connection);
                connection.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void closeAll() {
        try {
            server.close();
            synchronized (connections) {
                for (Connection connection : connections) {
                    connection.close();
                }
            }
        } catch (Exception e) {
            System.err.println("Потоки не были закрыты!");
        }
    }

    private class Connection extends Thread {
        private BufferedReader in;
        private PrintWriter out;
        private Socket socket;
        private GameLogic game;
        private String name;
        private Server server;

        public Connection(Socket socket, Server server) {
            name = "anon";
            this.socket = socket;
            this.server = server;
            try {
                in = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

            } catch (IOException e) {
                e.printStackTrace();
                close();
            }
        }

        @Override
        public void run() {
            try {
                name = in.readLine();
                points.put(name, 0);

                synchronized (connections) {
                    for (Connection connection : connections) {
                        (connection).out.println(name + " подключился");
                    }
                }

                if (connections.size() == N) {
                    game = new GameLogic();
                    game.setDaemon(true);
                    game.start();
                }

                while (true) {
                    String message = in.readLine();
                    if ("exit".equalsIgnoreCase(message))
                        break;

                    synchronized (connections) {
                        if (message.contains("v|")) {
                            answerPull.put(message.substring(2), name);

                            for (Connection connection : connections) {
                                (connection).out.println(name + ": выбрал " + message.substring(2));
                            }
                            if (token < connections.size())
                                connections.get(token).out.println("v|" + message.substring(2));

                        } else if (message.contains("w|")) {
                            server.flag = true;

                            //System.out.println(answerPull);
                            String name = answerPull.get(message.substring(2));
                            //System.out.println(name);
                            int counter = points.get(name) == null ? 0 : points.get(name);
                            points.put(name, counter + 1);


                            for (Connection connection : connections) {
                                String report = points.toString();
                                //System.out.println(report);
                                (connection).out.println("r|" + report.substring(1, report.length() - 1).replace("=", " - "));
                            }

                        } else {
                            for (Connection connection : connections) {
                                (connection).out.println(name + ": " + message);
                            }
                        }
                    }
                }

                synchronized (connections) {
                    for (Connection connection : connections) {
                        (connection).out.println(name + " has left");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close();
            }
        }


        public void close() {
            try {
                in.close();
                out.close();
                socket.close();

                connections.remove(this);
                if (connections.size() == 0) {
                    game.setAlive(false);
                    Server.this.closeAll();
                    System.exit(0);
                }
            } catch (Exception e) {
                System.err.println("Потоки не были закрыты!");
            }
        }
    }

    private class GameLogic extends Thread {
        private boolean alive;
        private Repo repo;
        private int qCounter;

        public GameLogic() {
            alive = true;
            repo = new Repo();
            qCounter = 0;
        }

        public void setAlive(boolean alive) {
            this.alive = alive;
        }

        @Override
        public void run() {
            Random random = new Random();
            long t1 = System.currentTimeMillis();
            long t2 = System.currentTimeMillis();
            int delay = 30000;
            int delay2 = 5000;

            while (alive) {
                if (flag || System.currentTimeMillis() - t2 > delay2) {
                    t2 = System.currentTimeMillis();
                    synchronized (connections) {
                        for (Connection connection : connections) {
                            connection.out.println("n|" + connections.size());
                        }
                    }
                }
                if (flag || System.currentTimeMillis() - t1 > delay) {
                    synchronized (connections) {
                        if (System.currentTimeMillis() - t1 > delay) {
                            if (token < connections.size())
                                connections.get(token).out.println("o|");
                        }
                        t1 = System.currentTimeMillis();
                        flag = false;


                        token = random.nextInt(connections.size());

                        connections.get(token).out.println("t|");

                        for (Connection connection : connections) {
                            connection.out.println("l|" + connections.get(token).name);
                        }

                        String quest = repo.choiceQ();
                        qCounter++;
                        for (Connection connection : connections) {
                            (connection).out.println("q|" + quest);
                            String report = points.toString();
                            (connection).out.println("r|" + report.substring(1, report.length() - 1).replace("=", " - "));
                        }
                        if (qCounter == 1) {
                            for (Connection connection : connections) {
                                (connection).out.println("a|" + repo.choiceA());
                            }
                        } else {
                            for (Connection connection : connections) {
                                (connection).out.println("c|" + repo.choiceC());
                            }
                        }
                    }
                }
            }
        }
    }
}