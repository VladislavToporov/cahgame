package main.java.gameserverCAH;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import main.java.gameserverCAH.service.Server;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Optional;

public class ServerStage extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
//        Media sound = new Media(ClassLoader.getSystemClassLoader().getResource("resourses/song.mp3").toString());
//        MediaPlayer mediaPlayer = new MediaPlayer(sound);
//        mediaPlayer.play();

        TextInputDialog dialog = new TextInputDialog("2 1234");
        dialog.setTitle("Настройки");
        dialog.setHeaderText(null);
        dialog.setContentText("Введите количество игроков и порт через пробел");

        Optional<String> result = dialog.showAndWait();
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        result.ifPresent(res -> {
            alert.setTitle("IP сервера и порт для подключений");
            alert.setHeaderText("Не закрывайте это окно, пока участники подключаются");
            String[] data = res.split(" ");
            try {
                String ipAddress = Inet4Address.getLocalHost().getHostAddress();
                alert.setContentText("IP: " + ipAddress + ", port: " + data[1]);
                alert.show();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            new Server(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
        });
    }
}
