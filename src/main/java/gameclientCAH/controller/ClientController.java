package main.java.gameclientCAH.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import main.java.gameclientCAH.service.ChatListenerThread;
import main.java.gameclientCAH.service.Client;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;


public class ClientController implements ClientControllerInterface {

    private PrintWriter writer;
    private BufferedReader reader;
    private Client client;
    private ChatListenerThread resender;

    @FXML
    private TextArea chat;
    @FXML
    private TextField messageField;
    @FXML
    private Text playersCounter;
    @FXML
    private Text question, timer, players, leader, label;
    @FXML
    private Text c1, c2, c3, c4, c5, c6, v1, v2, v3, v4, v5, v6, v7, v8, v9;
    private List<Text> cards;
    private List<Text> variants;

    public ClientController(BufferedReader reader, PrintWriter writer, Client client) {
        this.reader = reader;
        this.writer = writer;
        this.client = client;
    }

    @FXML
    public void initialize() {
        playersCounter.textProperty().addListener(event -> {
            if ("1".equals(playersCounter.getText()))
                gameOver();
        });
        messageField.setOnAction(event -> sendMessage());
        chat.textProperty().addListener(event -> chat.setScrollTop(Double.MAX_VALUE));
        cards = Arrays.asList(c1, c2, c3, c4, c5, c6);
        for (Text b : cards) {
            b.setOnMouseClicked(event -> {
                if (!client.getqFlag()) {
                    writer.println("v|" + b.getText());
                    b.setText("");
                    for (Text bu : cards) {
                        bu.setDisable(true);
                    }
                }
            });
        }

        variants = Arrays.asList(v1, v2, v3, v4, v5, v6, v7, v8, v9);
        for (Text b : variants)
            b.setOnMouseClicked(event -> {
                if (client.getqFlag()) {
                    client.setqFlag(false);
                    writer.println("w|" + b.getText());
                    writer.println(question.getText().replace("...",
                            b.getText()));
                    for (Text c : variants) {
                        c.setText("");
                        c.setDisable(true);
                        c.setVisible(false);
                    }
                }
            });

        resender = new ChatListenerThread(chat, reader, cards, question, client, timer, players, leader, variants, label, playersCounter);
        resender.setDaemon(true);
        resender.start();
    }

    @Override
    public void gameOver() {
        resender.disable();
        question.setText("Game over");
        timer.setText("");
        for (Text b : cards) {
            b.setText("");
        }
        label.setText("");
        for (Text b : variants) {
            b.setText("");
        }
    }

    @Override
    @FXML
    public void sendMessage() {
        String message = messageField.getText();
        if ("".equals(message))
            return;

        messageField.setText("");
        if (!"exit".equalsIgnoreCase(message))
            writer.println(message);
        else
            close();
    }

    @Override
    public void close() {
        writer.println("exit");
        resender.disable();
        client.close();
        System.exit(0);
    }

}
