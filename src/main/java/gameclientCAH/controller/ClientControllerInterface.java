package main.java.gameclientCAH.controller;

public interface ClientControllerInterface {
    void gameOver();

    void sendMessage();

    void close();
}
