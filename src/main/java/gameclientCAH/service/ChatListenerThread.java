package main.java.gameclientCAH.service;

import javafx.scene.control.TextArea;
import javafx.scene.text.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class ChatListenerThread extends Thread {
    private TextArea ta;
    private BufferedReader br;
    private boolean alive = true;
    private List<Text> cards;
    private Text label;
    private Client client;
    private Text timer;
    private Text players;
    private Text leader;
    private boolean isBegin = true;
    private List<Text> variants;
    private int counter = 0;
    private Text labelQueue;
    private Text playerCounter;
    private TimerThread timerThread;

    public ChatListenerThread(TextArea ta, BufferedReader br, List<Text> cards, Text label,
                              Client client, Text timer, Text players, Text leader, List<Text> variants,
                              Text labelQueue, Text playersCounter) {
        this.ta = ta;
        this.br = br;
        this.cards = cards;
        this.label = label;
        this.client = client;
        this.timer = timer;
        this.players = players;
        this.leader = leader;
        this.variants = variants;
        this.labelQueue = labelQueue;
        this.playerCounter = playersCounter;
    }

    public TimerThread getTimerThread() {
        return timerThread;
    }

    public void disable() {
        timerThread.setGo(false);
        alive = false;
    }

    @Override
    public void run() {
        try {
            while (alive) {
                String message = br.readLine();

                if (message != null && message.contains("t|")) { // token
                    client.setqFlag(true);
                    counter = 0;

                } else if (message != null && message.contains("q|")) { // question
                    if (isBegin) {
                        isBegin = false;
                        counter = 30;

                        timerThread = new TimerThread();
                        timerThread.setDaemon(true);
                        timerThread.start();
                    }
                    timerThread.setAlive(false);
                    label.setText(message.substring(2));


                    if (client.getqFlag()) {
                        labelQueue.setText("Выберите лучший ответ.");
                    } else {
                        labelQueue.setText("");
                    }

                } else if (message != null && message.contains("c|")) { // card
                    String card = message.substring(2);
                    for (Text c : cards) {
                        if ("".equals(c.getText())) {
                            c.setText(card);
                        }
                        c.setDisable(false);
                    }
                } else if (message != null && message.contains("a|")) { // answerPull
                    String[] array = message.substring(2).split("\\|");

                    for (int i = 0; i < array.length; i++) {
                        cards.get(i).setText(array[i]);
                        cards.get(i).setDisable(false);
                        cards.get(i).setVisible(true);
                    }
                } else if (message != null && message.contains("v|")) { // variant
                    String array = message.substring(2);
                    variants.get(counter % 6).setText(array);
                    variants.get(counter % 6).setDisable(false);
                    variants.get(counter % 6).setVisible(true);
                    counter++;

                } else if (message != null && message.contains("r|")) { // report
                    players.setText(message.substring(2).replace(", ", "\n"));
                } else if (message != null && message.contains("l|")) { // leader
                    leader.setText(message.substring(2));
                } else if (message != null && message.contains("o|")) { // overtime
                    leader.setText("");
                    for (Text variant : variants) {
                        variant.setText("");
                        variant.setDisable(true);
                        variant.setVisible(false);
                        label.setText("");
                    }
                } else if (message != null && message.contains("n|")) { // number
                    playerCounter.setText(message.substring(2));
                } else if (message != null) {
                    ta.setText(ta.getText() + message + "\n");
                }
            }
        } catch (IOException e) {
            System.err.println("Ошибка при получении сообщения.");
            System.exit(1);
        }
    }


    private class TimerThread extends Thread {
        private int counter;
        private volatile boolean alive;
        private boolean go;

        public TimerThread() {
            counter = 30;
            alive = true;
            go = true;
        }

        public void setGo(boolean go) {
            this.go = go;
        }

        public void setAlive(boolean alive) {
            this.alive = alive;
        }

        @Override
        public void run() {
            long t1 = System.currentTimeMillis();
            while (go) {
                if (counter < 1)
                    client.setqFlag(false);
                counter = 30;
                while (counter > 0 && go) {
                    if (!alive) {
                        alive = true;
                        timer.setText("");
                        break;
                    }
                    if (System.currentTimeMillis() - t1 > 1000) {
                        t1 = System.currentTimeMillis();
                        timer.setText(String.valueOf((counter--)));
                    }
                }
            }
        }
    }
}