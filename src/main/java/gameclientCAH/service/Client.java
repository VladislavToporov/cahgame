package main.java.gameclientCAH.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private BufferedReader in;
    private PrintWriter out;
    private Socket socket;
    private volatile boolean qFlag;

    public Client(String ip, int port, String name) {
        if (port <= 0)
            throw new IllegalArgumentException("Wrong port");
        if (!("localhost".equals(ip) || ip.matches("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}")))
            throw new IllegalArgumentException("Wrong ip");
        if ("".equals(name))
            throw new IllegalArgumentException("Wrong name");
        qFlag = false;
        try {
            socket = new Socket(ip, port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            out.println(name);
        } catch (Exception e) {
            System.err.println("Не удалось подключиться к серверу");
            System.exit(1);
        }
    }

    public boolean getqFlag() {
        return qFlag;
    }

    public void setqFlag(boolean flag) {
        this.qFlag = flag;
    }

    public BufferedReader getIn() {
        return in;
    }

    public PrintWriter getOut() {
        return out;
    }

    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (Exception e) {
            System.err.println("Потоки не были закрыты!");
        }
    }

}