package main.java.gameclientCAH;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import main.java.gameclientCAH.controller.ClientController;
import main.java.gameclientCAH.service.Client;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Arrays;


public class ClientStage extends Application {
    private PrintWriter writer;
    private Client client;
    private TextField ip;
    private TextField username;
    private TextField port;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/view.fxml"));


        String[] data = auth();
        if ("".equals(data[0]))
            System.exit(0);

        client = new Client(data[0], Integer.parseInt(data[1]), data[2]);
        writer = client.getOut();
        BufferedReader reader = client.getIn();
        ClientController controller = new ClientController(reader, writer, client);
        loader.setController(controller);
        loader.load();

        primaryStage.setResizable(false);
        primaryStage.setTitle("Cards against humanity");
        primaryStage.setScene(new Scene(loader.getRoot(), 800, 650));
        primaryStage.show();
    }

    @Override
    public void stop() {
        writer.println("exit");
        client.close();

    }

    private String[] auth() {
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Auth Dialog");
        dialog.setHeaderText("Введите ip и порт");


        ButtonType playButtonType = new ButtonType("OK GO", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(playButtonType, ButtonType.CANCEL);


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        ip = new TextField();
        ip.setPromptText("localhost");
        port = new TextField();
        port.setPromptText("1234");
        username = new TextField();
        username.setPromptText("anon");


        grid.add(new Label("IP:"), 0, 0);
        grid.add(ip, 1, 0);

        grid.add(new Label("port:"), 0, 1);
        grid.add(port, 1, 1);


        grid.add(new Label("Username:"), 0, 2);
        grid.add(username, 1, 2);


        Node playButton = dialog.getDialogPane().lookupButton(playButtonType);
        playButton.setDisable(true);

        validateFields(playButton);

        dialog.getDialogPane().setContent(grid);
        Platform.runLater(ip::requestFocus);
        dialog.showAndWait();
        String[] res = new String[]{ip.getText(), port.getText(), username.getText()};
        System.out.println(Arrays.toString(res));
        return res;
    }

    private void validateFields(Node playButton) {
        username.textProperty().addListener((observable, oldValue, newValue) -> {
            ip.textProperty().addListener((observable2, oldValue2, newValue2) -> {
                port.textProperty().addListener((observable3, oldValue3, newValue3) -> {
                    playButton.setDisable(newValue.trim().isEmpty() || newValue2.trim().isEmpty() || newValue3.trim().isEmpty());
                });
            });
        });

        ip.textProperty().addListener((observable, oldValue, newValue) -> {
            port.textProperty().addListener((observable2, oldValue2, newValue2) -> {
                username.textProperty().addListener((observable3, oldValue3, newValue3) -> {
                    playButton.setDisable(newValue.trim().isEmpty() || newValue2.trim().isEmpty() || newValue3.trim().isEmpty());
                });
            });
        });

        port.textProperty().addListener((observable, oldValue, newValue) -> {
            ip.textProperty().addListener((observable2, oldValue2, newValue2) -> {
                username.textProperty().addListener((observable3, oldValue3, newValue3) -> {
                    playButton.setDisable(newValue.trim().isEmpty() || newValue2.trim().isEmpty() || newValue3.trim().isEmpty());
                });
            });
        });
    }
}




