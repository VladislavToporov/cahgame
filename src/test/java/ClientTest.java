package test.java;

import main.java.gameclientCAH.service.Client;
import org.junit.Test;

public class ClientTest {

    @Test(expected = IllegalArgumentException.class)
    public void testThatClientHaveWrongIP() { new Client("192.168.10", 100, "a"); }
    @Test(expected = IllegalArgumentException.class)
    public void testThatClientHaveWrongPort() {
        new Client("localhost", 0, "a");
    }
    @Test(expected = IllegalArgumentException.class)
    public void testThatClientHaveWrongName() {
        new Client("192.168.0.1", 100, "");
    }
}
