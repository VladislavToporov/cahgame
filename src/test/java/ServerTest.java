package test.java;

import main.java.gameserverCAH.service.Server;
import org.junit.Test;

public class ServerTest {

    @Test(expected = IllegalArgumentException.class)
    public void testThatServerHaveWrongPort() { new Server(2, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThatServerHaveWrongN() { new Server(1, 1234);
    }
}
