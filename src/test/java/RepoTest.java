package test.java;

import main.java.gameserverCAH.service.Repo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class RepoTest {
    private Repo repo;
    private int pullSize = 6;
    @Before
    public void createRepo() {
        repo = new Repo();
    }

    @Test
    public void testThatAnswersListIsNotEmpty() {
        List<String> answers = repo.fillAnswers();
        Assert.assertTrue(answers.size() > 0);
    }


    @Test
    public void testThatQuestionListIsNotEmpty() {
        List<String> questions = repo.fillQuestions();
        Assert.assertTrue(questions.size() > 0);
    }

    @Test
    public void testThatNextAnswerIsNotNull() {
        String a = "";
        while (repo.getaIter().hasNext()) {
            a = repo.choiceC();
            if (a == null)
                break;
        }

        Assert.assertNotNull(a);
    }

    @Test
    public void testThatNextQuestionIsNotNull() {
        Repo repo = new Repo();
        String a = "";
        while (repo.getqIter().hasNext()) {
            a = repo.choiceQ();
            if (a == null)
                break;
        }

        Assert.assertNotNull(a);
    }

    @Test
    public void testThatNextAnswerPullSizeEqualsSix() {
        String a = repo.choiceA();
        System.out.println(a);
        Assert.assertEquals(a.split("\\|").length, pullSize);
    }
}
